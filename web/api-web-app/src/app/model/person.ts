export interface Person {
  name: string,
  prename: string,
  age?: number
}
