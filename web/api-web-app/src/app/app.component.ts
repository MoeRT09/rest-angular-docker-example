import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Person } from './model/person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient) {}
  title = 'api-web-app';
  people: Observable<Array<Person>>;

  newPerson: Person = {name: '', prename: ''}

  ngOnInit() {
    this.getPeople();
  }

  public getPeople() {
    this.people = this.http.get<Array<Person>>(environment.apiUrl + '/person');
  }

  public createPerson() {
    this.http.post(environment.apiUrl + "/person", this.newPerson).subscribe((result) => {
      console.log(result);
      this.getPeople();
    });
  }
}
