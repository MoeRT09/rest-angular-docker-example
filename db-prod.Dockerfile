FROM mariadb:latest

ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=apidb

COPY ./db-init /docker-entrypoint-initdb.d/