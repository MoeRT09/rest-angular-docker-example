FROM trion/ng-cli:9.1.9 AS build
COPY ./web/api-web-app /app
RUN npm install && \
    ng build --prod

FROM httpd:2.4
COPY --from=build /app/dist/api-web-app /usr/local/apache2/htdocs/