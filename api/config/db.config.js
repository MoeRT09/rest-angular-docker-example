module.exports = {
  database: process.env.DB_DATABASE || "apidb",
  host: process.env.DB_HOST || "localhost",
  username: process.env.DB_USERNAME || "root",
  password: process.env.DB_PASSWORD || "root",
  dialect: "mariadb"
  // and others
}