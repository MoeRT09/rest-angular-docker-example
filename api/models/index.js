const dbConfig = require("../config/db.config");
const { Sequelize } = require("sequelize");
const Person = require("./person.model");

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
  dialect: dbConfig.dialect,
  host: dbConfig.host
});

const db = {};

db.person = Person.init(sequelize);

sequelize.sync({alter: true});

module.exports = db;