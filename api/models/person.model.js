const { Model, DataTypes } = require("sequelize");

class Person extends Model {
  static init(sequelize) {
    return super.init({
      name: {type: DataTypes.STRING, allowNull: false},
      prename: {type: DataTypes.STRING, allowNull: false},
      age: {type: DataTypes.INTEGER}
    },
    {
      sequelize,
      modelName: 'people'
    });
  }
};

module.exports = Person;