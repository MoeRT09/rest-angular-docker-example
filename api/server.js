const express = require("express");
const cors = require("cors");

const app = express();

const corsOptions = {
  origin: process.env.CORS_ORIGIN || true
}

app.use(cors(corsOptions));
app.use(express.json());

app.get("/", (req, res) => {
  res.json({message: "API server is running"});
})

const PORT = process.env.PORT || 8080;

app.use(require("./routes/person.routes"));

app.listen(PORT, () => {
  console.log(`Web API Server in Docker is running at port ${PORT}`);
});