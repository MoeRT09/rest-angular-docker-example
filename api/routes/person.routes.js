const personController = require("../controllers/person.controller");
const router = require("express").Router();

router.post("/person", personController.create);
router.get("/person", personController.findAll);

module.exports = router;