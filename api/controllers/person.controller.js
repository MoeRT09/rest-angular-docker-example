const { Op } = require("sequelize");
const db = require("../models");
const Person = db.person;

async function create(req, res) {
  if(!req.body.name || !req.body.prename) {
    res.status(400).send({
      message: "Name and prename have to be provided"
    });
    return;
  }

  const person = {
    name: req.body.name,
    prename: req.body.prename,
    age: req.body.age
  }
  try {
    const result = await Person.create(person);
    res.send(result);

  }
  catch(error) {
    res.status(500).send({
      message: error.message || "Error while creating person"
    })
  }
}

async function findAll(req, res) {
  const name = req.query.name;

  const condition = name ? {name: { [Op.like]: `%${name}%`} } : null;

  try {
    const result = await Person.findAll({ where: condition });
    res.send(result);
  }
  catch(error) {
    res.status(500).send({
      message: error.message || "Error while retrieving people"
    });
  }
}

module.exports = {create, findAll};