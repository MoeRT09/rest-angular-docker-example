FROM node:12

EXPOSE 8080

ENV DB_HOST=localhost

COPY ./api /app/
WORKDIR /app
RUN npm install --only=production

CMD bash -c "until echo '' > /dev/tcp/${DB_HOST}/3306; do sleep 1; done && npm run start"